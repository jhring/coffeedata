-- This file should undo anything in `up.sql`
DROP TABLE espresso_brews;
DROP TABLE coffee_beans;
DROP TABLE users;