CREATE TABLE users (
                       id SERIAL PRIMARY KEY,
                       username VARCHAR NOT NULL,
                       email VARCHAR NOT NULL,
                       password VARCHAR NOT NULL,

                       UNIQUE(username),
                       UNIQUE(email)
);

-- CREATE TYPE roast_type AS ENUM ('Light', 'Medium', 'MediumDark', 'Dark');

CREATE TABLE coffee_beans
(
    id           SERIAL PRIMARY KEY,
    roaster_name VARCHAR    NOT NULL,
    roast_name   VARCHAR    NOT NULL,
    roast_type   VARCHAR    NOT NULL,
    submitter    INT        NOT NULL,
    created_at   TIMESTAMP NOT NULL,

    CONSTRAINT fk_submitter_coffee
        FOREIGN KEY(submitter)
            REFERENCES users(id)
);

CREATE UNIQUE INDEX coffee_roast
    ON coffee_beans(roaster_name, roast_name);


CREATE TABLE espresso_brews (
                       id SERIAL PRIMARY KEY,
                       roast INT NOT NULL,
                       machine VARCHAR NOT NULL,
                       grinder VARCHAR NOT NULL,
                       dose REAL NOT NULL,
                       brew_yield REAL NOT NULL,
                       grind_step SMALLINT NOT NULL,
                       brew_time REAL NOT NULL,
                       temperature_step SMALLINT NOT NULL,
                       taste SMALLINT NOT NULL,
                       thickness SMALLINT NOT NULL,
                       flavor SMALLINT NOT NULL,
                       aftertaste SMALLINT NOT NULL,
                       submitter INT NOT NULL,
                       created_at TIMESTAMP NOT NULL,

                       CONSTRAINT fk_submitter
                           FOREIGN KEY(submitter)
                               REFERENCES users(id),

                       CONSTRAINT fk_roast
                            FOREIGN KEY(roast)
                                REFERENCES coffee_beans(id)
);
