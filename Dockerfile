#ARG POSTGRES_PASSWORD=$POSTGRES_PASSWORD

FROM rust:1.60 AS builder
RUN apt-get update && apt-get install -y clang
WORKDIR /coffee_data
COPY Cargo.toml Cargo.lock ./
COPY src ./src/
COPY migrations ./migrations/
RUN cargo build --release


# install and setup DB
FROM ubuntu:latest

RUN apt-get update && apt-get install -y libpq-dev
COPY css /app/css
COPY templates /app/templates
COPY .env /app/
COPY --from=builder /coffee_data/target/release/coffee_data /app/
WORKDIR /app
EXPOSE 8001
ENTRYPOINT ["/app/coffee_data"]