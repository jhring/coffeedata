table! {
    coffee_beans (id) {
        id -> Int4,
        roaster_name -> Varchar,
        roast_name -> Varchar,
        roast_type -> Varchar,
        submitter -> Int4,
        created_at -> Timestamp,
    }
}

table! {
    espresso_brews (id) {
        id -> Int4,
        roast -> Int4,
        machine -> Varchar,
        grinder -> Varchar,
        dose -> Float4,
        brew_yield -> Float4,
        grind_step -> Int2,
        brew_time -> Float4,
        temperature_step -> Int2,
        taste -> Int2,
        thickness -> Int2,
        flavor -> Int2,
        aftertaste -> Int2,
        submitter -> Int4,
        created_at -> Timestamp,
    }
}

table! {
    users (id) {
        id -> Int4,
        username -> Varchar,
        email -> Varchar,
        password -> Varchar,
    }
}

joinable!(coffee_beans -> users (submitter));
joinable!(espresso_brews -> coffee_beans (roast));
joinable!(espresso_brews -> users (submitter));

allow_tables_to_appear_in_same_query!(
    coffee_beans,
    espresso_brews,
    users,
);
