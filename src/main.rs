// todo prevent dup submissions by time
// todo handle tera errors
// todo clean up internal errors
#[macro_use]
extern crate diesel;
extern crate argonautica;
#[macro_use]
extern crate diesel_migrations;

pub mod models;
pub mod schema;

use actix_identity::{CookieIdentityPolicy, Identity, IdentityService};
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use actix_web::middleware::Logger;
use actix_files as fs;
use argonautica::Verifier;
use diesel::dsl::exists;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use diesel::r2d2::ConnectionManager;
use diesel::result::Error;
use diesel::select;
use diesel_migrations::embed_migrations;

type Pool = r2d2::Pool<ConnectionManager<PgConnection>>;

use dotenv::dotenv;
use serde::{Deserialize, Serialize};
use tera::{Context, Tera};

use models::{EspressoBrew, LoginUser, NewEspressoBrew, NewUser, User, CoffeeRoast, CoffeeSubmissionForm, NewCoffeeRoast};
use crate::schema::coffee_beans::dsl::coffee_beans;

use crate::schema::users::email;

embed_migrations!();

#[derive(Debug)]
pub enum ServerError {
    Argonautic,
    Diesel,
    Environment,
    R2D2,
}

impl std::fmt::Display for ServerError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Test")
    }
}

impl From<std::env::VarError> for ServerError {
    fn from(err: std::env::VarError) -> ServerError {
        log::error!("{:?}", err);
        ServerError::Environment
    }
}

impl From<r2d2::Error> for ServerError {
    fn from(err: r2d2::Error) -> ServerError {
        log::error!("{:?}", err);
        ServerError::R2D2
    }
}

impl From<argonautica::Error> for ServerError {
    fn from(err: argonautica::Error) -> ServerError {
        log::error!("{:?}", err);
        ServerError::Argonautic
    }
}

impl From<diesel::result::Error> for ServerError {
    fn from(err: diesel::result::Error) -> ServerError {
        log::error!("{:?}", err);
        match err {
            _ => ServerError::Diesel
        }
    }
}

impl actix_web::error::ResponseError for ServerError {
    fn error_response(&self) -> HttpResponse {
        match self {
            ServerError::Argonautic => {
                HttpResponse::InternalServerError().json("Argonautica Error.")
            }
            ServerError::Diesel => HttpResponse::InternalServerError().json("Diesel Error."),
            ServerError::Environment => {
                HttpResponse::InternalServerError().json("Environment Error.")
            }
            ServerError::R2D2 => HttpResponse::InternalServerError().json("R2D2 Error."),
        }
    }
}

#[derive(Serialize, Default)]
struct Alert {
    level: String,
    message: String
}

#[derive(Deserialize)]
pub struct EspressoSubmissionForm {
    roast: i32,
    machine: String,
    grinder: String,
    dose: f32,
    brew_yield: f32,
    grind_step: i16,
    brew_time: f32,
    temperature_step: i16,
    taste: i16,
    thickness: i16,
    flavor: i16,
    aftertaste: i16,
}

async fn login(tera: web::Data<Tera>, id: Identity) -> Result<HttpResponse, ServerError> {
    let mut data = Context::new();

    if let Some(_id) = id.identity() {
        let alert = Alert {
            level: "warning".to_string(),
            message: "Already logged in.".to_string()
        };
        data.insert("alert", &alert);
        let rendered = tera.render("login.html", &data).unwrap();
        Ok(HttpResponse::Ok().body(rendered))
    } else {
        data.insert("page", "login");
        let rendered = tera.render("login.html", &data).unwrap();
        Ok(HttpResponse::Ok().body(rendered))
    }
}

async fn logout(tera: web::Data<Tera>, id: Identity) -> impl Responder {
    id.forget();
    let mut data = Context::new();
    let alert = Alert {
        level: "success".to_string(),
        message: "Logout Successful.".to_string()
    };
    data.insert("alert", &alert);
    let rendered = tera.render("base.html", &data).unwrap();
    HttpResponse::Ok().body(rendered)
}

async fn submission(tera: web::Data<Tera>, identity: Identity, pool: web::Data<Pool>) -> impl Responder {
    let mut data = Context::new();
    data.insert("page", "submission");

    let connection = pool.get().unwrap();

    if let Some(_id) = identity.identity() {
        use crate::schema::coffee_beans::{roast_name, roaster_name, id};

        let roasts: Vec<(String, String, i32)> = coffee_beans.select((roaster_name, roast_name, id)).order((roaster_name.desc(), roast_name.desc())).load(&connection).unwrap();
        println!("{:?}", roasts);
        data.insert("roasts", &roasts);

        let rendered = tera.render("submission.html", &data).unwrap();
        return HttpResponse::Ok().body(rendered);
    }
    let alert = Alert {
        level: "danger".to_string(),
        message: "You must be logged in to access this page".to_string()
    };
    data.insert("alert", &alert);
    let rendered = tera.render("base.html", &data).unwrap();
    HttpResponse::Unauthorized().body(rendered)
}

async fn coffee(tera: web::Data<Tera>, id: Identity) -> impl Responder {
    let mut data = Context::new();
    data.insert("page", "coffee");

    if let Some(_id) = id.identity() {
        let rendered = tera.render("coffee.html", &data).unwrap();
        return HttpResponse::Ok().body(rendered);
    }
    let alert = Alert {
        level: "danger".to_string(),
        message: "You must be logged in to access this page".to_string()
    };
    data.insert("alert", &alert);
    let rendered = tera.render("base.html", &data).unwrap();
    HttpResponse::Unauthorized().body(rendered)
}

async fn process_submission(
    tera: web::Data<Tera>,
    form_data: web::Form<EspressoSubmissionForm>,
    id: Identity,
    pool: web::Data<Pool>,
) -> Result<HttpResponse, ServerError> {
    let mut alert = Alert::default();
    let mut data = Context::new();

    if let Some(id) = id.identity() {
        use schema::users::dsl::{username, users};

        let connection = pool.get()?;
        let user: Result<User, diesel::result::Error> =
            users.filter(username.eq(id)).first(&connection);

        return match user {
            Ok(u) => {
                let new_brew = NewEspressoBrew::from_submission_form(form_data.into_inner(), u.id);

                use schema::espresso_brews;

                diesel::insert_into(espresso_brews::table)
                    .values(&new_brew)
                    .get_result::<EspressoBrew>(&connection)?;

                alert.level = "success".to_string();
                alert.message = "Submitted!".to_string();
                data.insert("alert", &alert);
                let rendered = tera.render("base.html", &data).unwrap();
                Ok(HttpResponse::Ok().body(rendered))
            }
            Err(_) => {
                alert.level = "danger".to_string();
                alert.message = "An error occurred please logout and try again.".to_string();
                data.insert("alert", &alert);
                let rendered = tera.render("base.html", &data).unwrap();
                Ok(HttpResponse::Ok().body(rendered))
            }
        };
    }
    Ok(HttpResponse::Unauthorized().body("User not logged in."))
}

async fn process_coffee(
    tera: web::Data<Tera>,
    form_data: web::Form<CoffeeSubmissionForm>,
    id: Identity,
    pool: web::Data<Pool>,
) -> Result<HttpResponse, ServerError> {
    let mut alert = Alert::default();
    let mut data = Context::new();

    if let Some(id) = id.identity() {
        use schema::users::dsl::{username, users};

        let connection = pool.get()?;
        let user: Result<User, diesel::result::Error> =
            users.filter(username.eq(id)).first(&connection);

        return match user {
            Ok(u) => {
                let new_roast = NewCoffeeRoast::from_submission_form(form_data.into_inner(), u.id);

                use schema::coffee_beans;

                diesel::insert_into(coffee_beans::table)
                    .values(&new_roast)
                    .get_result::<CoffeeRoast>(&connection)?;

                alert.level = "success".to_string();
                alert.message = "Submitted!".to_string();
                data.insert("alert", &alert);
                let rendered = tera.render("base.html", &data).unwrap();
                Ok(HttpResponse::Ok().body(rendered))
            }
            Err(_) => {
                alert.level = "danger".to_string();
                alert.message = "An error occurred please logout and try again.".to_string();
                data.insert("alert", &alert);
                let rendered = tera.render("base.html", &data).unwrap();
                Ok(HttpResponse::Ok().body(rendered))
            }
        };
    }
    Ok(HttpResponse::Unauthorized().body("User not logged in."))
}

async fn process_login(
    tera: web::Data<Tera>,
    data: web::Form<LoginUser>,
    id: Identity,
    pool: web::Data<Pool>,
) -> Result<HttpResponse, ServerError> {
    use schema::users::dsl::{username, users};

    let connection = pool.get()?;
    let user = users
        .filter(username.eq(&data.username))
        .first::<User>(&connection);

    let mut alert = Alert::default();
    let mut render_page = "base.html";

    match user {
        Ok(u) => {
            dotenv().ok();
            let secret = std::env::var("SECRET_KEY")?;

            let valid = Verifier::default()
                .with_hash(u.password)
                .with_password(data.password.clone())
                .with_secret_key(secret)
                .verify()?;

            if valid {
                let session_token = String::from(u.username);
                id.remember(session_token);
                alert.level = "success".to_string();
                alert.message = format!("Logged in: {}", data.username).to_string();
            } else {
                    render_page = "login.html";
                    alert.level = "danger".to_string();
                    alert.message = format!("Incorrect username, password combination.").to_string();
            }
        },
        Err(e) => {
            match e {
                Error::NotFound => {
                    render_page = "login.html";
                    alert.level = "danger".to_string();
                    alert.message = format!("Incorrect username, password combination.").to_string();
                }
                _ => return Err(ServerError::from(e))
            }
        }
    }

    let mut data = Context::new();
    data.insert("page", "login");
    data.insert("alert", &alert);
    let rendered = tera.render(render_page, &data).unwrap();
    Ok(HttpResponse::Ok().body(rendered))
}

async fn process_signup(tera: web::Data<Tera>, data: web::Form<NewUser>, pool: web::Data<Pool>) -> Result<HttpResponse, ServerError> {
    use schema::users::table as users_table;
    use schema::users::dsl::{username, users};

    let connection = pool.get()?;
    let new_user = NewUser::new(data.into_inner())?;

    let user_exist: bool = select(exists(users.filter(username.eq(&new_user.username)))).get_result(&connection)?;
    let email_exist: bool = select(exists(users.filter(email.eq(&new_user.email)))).get_result(&connection)?;

    let mut alert = Alert::default();

    if user_exist {
        alert.level = "danger".to_string();
        alert.message = "Username already taken".to_string();
    } else if email_exist {
        alert.level = "danger".to_string();
        alert.message = "An account with that email already exists.".to_string();
    } else {
        diesel::insert_into(users_table)
            .values(&new_user)
            .get_result::<User>(&connection)?;

        alert.level = "success".to_string();
        alert.message = format!("Successfully signed up: {}. You may now login.", new_user.username).to_string();
    }

    let mut data = Context::new();
    data.insert("page", "signup");
    data.insert("alert", &alert);
    let rendered = tera.render("signup.html", &data).unwrap();
    Ok(HttpResponse::Ok().body(rendered))
}

async fn signup(tera: web::Data<Tera>) -> Result<HttpResponse, ServerError> {
    let mut data = Context::new();
    data.insert("page", "signup");

    let rendered = tera.render("signup.html", &data).unwrap();
    Ok(HttpResponse::Ok().body(rendered))
}

async fn index(tera: web::Data<Tera>, pool: web::Data<Pool>) -> Result<HttpResponse, ServerError> {
    use schema::espresso_brews::dsl::espresso_brews;
    use schema::users::dsl::users;

    let connection = pool.get()?;
    let all_brews: Vec<(EspressoBrew, User)> = espresso_brews
        .inner_join(users)
        .load(&connection)
        .expect("Error retrieving all posts.");

    let mut data = Context::new();
    data.insert("page", "home");
    data.insert("brews_users", &all_brews);

    let rendered = tera.render("index.html", &data).unwrap();
    Ok(HttpResponse::Ok().body(rendered))
}

async fn not_found(tera: web::Data<Tera>) -> Result<HttpResponse, ServerError> {
    let data = Context::new();
    let rendered = tera.render("404.html", &data).unwrap();
    Ok(HttpResponse::NotFound().body(rendered))
}


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();
    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");

    let manager = ConnectionManager::<PgConnection>::new(database_url);
    let pool = r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create postgres pool.");

    env_logger::init();

    let connection = pool.get().unwrap();
    embedded_migrations::run(&connection);

    HttpServer::new(move || {
        let tera = web::Data::new(Tera::new("templates/**/*").unwrap());
        let pool = web::Data::new(pool.clone());
        App::new()
            .wrap(Logger::default())
            .wrap(IdentityService::new(
                CookieIdentityPolicy::new(&[0; 32])
                    .name("auth-cookie")
                    .secure(false),
            ))
            .service(fs::Files::new("css", "css").show_files_listing())
            .app_data(tera)
            .app_data(pool)
            .route("/", web::get().to(index))
            .route("/home", web::get().to(index))
            .route("/signup", web::get().to(signup))
            .route("/signup", web::post().to(process_signup))
            .route("/login", web::get().to(login))
            .route("/login", web::post().to(process_login))
            .route("/logout", web::get().to(logout))
            .route("/submission", web::get().to(submission))
            .route("/submission", web::post().to(process_submission))
            .route("/coffee", web::get().to(coffee))
            .route("/coffee", web::post().to(process_coffee))
            .default_service(web::route().to(not_found))
    })
        .bind("0.0.0.0:8001")?
        .run()
        .await
}
