use crate::{EspressoSubmissionForm, ServerError};
use argonautica::Hasher;
use diesel::{Insertable, Queryable};
use dotenv::dotenv;
use serde::{Deserialize, Serialize};

use super::schema::{espresso_brews, users, coffee_beans};

#[derive(Queryable, Serialize)]
pub struct User {
    pub id: i32,
    pub username: String,
    pub email: String,
    pub password: String,
}

#[derive(Debug, Deserialize, Insertable)]
#[table_name = "users"]
pub struct NewUser {
    pub username: String,
    pub email: String,
    pub password: String,
}

impl NewUser {
    pub fn new(mut user: NewUser) -> Result<Self, ServerError> {
        dotenv().ok();

        // todo not handled in main
        let secret = std::env::var("SECRET_KEY")?;

        let hash = Hasher::default()
            .with_password(user.password)
            .with_secret_key(secret)
            .hash()?;
        user.password = hash;
        Ok(user)
    }
}

#[derive(Debug, Deserialize)]
pub struct LoginUser {
    pub username: String,
    pub password: String,
}

#[derive(Debug, Queryable, Serialize)]
pub struct EspressoBrew {
    pub id: i32,
    pub roast: i32,
    pub machine: String,
    pub grinder: String,
    pub dose: f32,
    pub brew_yield: f32,
    pub grind_step: i16,
    pub brew_time: f32,
    pub temperature_step: i16,
    pub taste: i16,
    pub thickness: i16,
    pub flavor: i16,
    pub aftertaste: i16,
    pub submitter: i32,
    pub created_at: chrono::NaiveDateTime,
}

#[derive(Serialize, Insertable)]
#[table_name = "espresso_brews"]
pub struct NewEspressoBrew {
    pub roast: i32,
    pub machine: String,
    pub grinder: String,
    pub dose: f32,
    pub brew_yield: f32,
    pub grind_step: i16,
    pub brew_time: f32,
    pub temperature_step: i16,
    pub taste: i16,
    pub thickness: i16,
    pub flavor: i16,
    pub aftertaste: i16,
    pub submitter: i32,
    pub created_at: chrono::NaiveDateTime,
}

#[derive(Debug, Queryable, Serialize)]
pub struct CoffeeRoast {
    pub id: i32,
    pub roaster_name: String,
    pub roast_name: String,
    pub roast_type: String,
    pub submitter: i32,
    pub created_at: chrono::NaiveDateTime,
}

#[derive(Serialize, Insertable)]
#[table_name = "coffee_beans"]
pub struct NewCoffeeRoast {
    pub roaster_name: String,
    pub roast_name: String,
    pub roast_type: String,
    pub submitter: i32,
    pub created_at: chrono::NaiveDateTime,
}

#[derive(Deserialize)]
pub struct CoffeeSubmissionForm {
    pub roaster_name: String,
    pub roast_name: String,
    pub roast_type: String,
}

impl NewCoffeeRoast {
    pub fn from_submission_form(form: CoffeeSubmissionForm, id: i32) -> Self {
        NewCoffeeRoast {
            roaster_name: form.roaster_name,
            roast_name: form.roast_name,
            roast_type: form.roast_type,
            submitter: id,
            created_at: chrono::Local::now().naive_utc(),
        }
    }
}

impl NewEspressoBrew {
    pub fn from_submission_form(form: EspressoSubmissionForm, id: i32) -> Self {
        NewEspressoBrew {
            roast: form.roast,
            machine: form.machine,
            grinder: form.grinder,
            dose: form.dose,
            brew_yield: form.brew_yield,
            grind_step: form.grind_step,
            brew_time: form.brew_time,
            temperature_step: form.temperature_step,
            taste: form.taste,
            thickness: form.thickness,
            flavor: form.flavor,
            aftertaste: form.aftertaste,
            submitter: id,
            created_at: chrono::Local::now().naive_utc(),
        }
    }
}
